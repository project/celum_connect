Configuration celum:connect Drupal 8/9 project (module)

INTRODUCTION
============
Drupal celum:connect is a Drupal 8 extension, which allows you to download assets from CELUM via the CELUM Asset Picker directly into the Drupal 8 filesystem and use it in a post, or just link it from celum in your Drupal 8 post.

REQUIREMENTS
============
Requires Drupal core config module and a CELUM Installation

INSTALLATION
============
Installs as usually Drupal module installation via Administration
or through `composer require drupal/celum_connect`.

CONFIGURATION
=============
The module provides a menu item under Administration >
Configuration > Development > Configuration > Celum:connect
