Custom.AssetPickerConfig = {
            endPoint: 'http://localhost:8080/appserver/cora',
            apiKey: 'kfn3a0ik1er3mrf4einn69fsq8',
            locale: 'de',
            searchScope: {
              rootNodes: [13]
            },
            requiredAssetData: ['fileInformation','versionInformation'],
            downloadFormats: {
              defaults: {
                unknown: 1,
                image: 1,
                document:1,
                video: 1,
                audio: 1,
                text: 1
                },
                supported: [1],
                additionalDownloadFormats: [1]
            },
            nrOfAllowedDownloadFormats: 99,
            forceDownloadSelection: true,
            keepSelectionOnExport: true
        };